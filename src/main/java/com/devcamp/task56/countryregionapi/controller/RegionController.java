package com.devcamp.task56.countryregionapi.controller;

import com.devcamp.task56.countryregionapi.model.Region;
import org.springframework.web.bind.annotation.*;
import com.devcamp.task56.countryregionapi.service.*;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class RegionController {
    @Autowired
    private RegionService regionService;

    @CrossOrigin
    @GetMapping("/region-info/{code}")
	public Region getRegionInfo(@PathVariable String code) {
        return regionService.getRegionByCode(code);
	}
}
